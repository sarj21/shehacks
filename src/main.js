import Vue from 'vue'
import VueRouter from 'vue-router'
import './plugins/vuetify'
import App from './App.vue'
import Main from './components/Main.vue'
import Schedule from './components/Schedule.vue'

Vue.use(VueRouter)
Vue.config.productionTip = false

const routes = [
  { path: '/', component: Main },
  { path: '/schedule', component: Schedule }
]

const router = new VueRouter({
  mode: 'history',
  routes: routes, // short for `routes: routes`
  scrollBehavior(to, from, savedPosition) {
    if (document.getElementById(to.hash)) {
      return {
        selector: document.getElementById(to.hash)
      }
    } else if (savedPosition) {
      return savedPosition
    } else if (from) {
      return {x: 0, y: 0}
    } 
  }
})

new Vue({
  el: '#app',
  router: router,
  render: h => h(App)
})
// .$mount('#app')
