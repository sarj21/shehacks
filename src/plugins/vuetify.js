import Vue from 'vue'
import Vuetify from 'vuetify/lib'
import 'vuetify/src/stylus/app.styl'

Vue.use(Vuetify, {
  iconfont: 'md',
  theme: {
    primary: '#a0ced3',
    secondary: '#7c0e37',
    accent: '#ff8566'
  }
})
